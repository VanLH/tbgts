<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200921135923 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE details (id INT AUTO_INCREMENT NOT NULL, an_order_id INT NOT NULL, product_id INT NOT NULL, total_invoiced DOUBLE PRECISION NOT NULL, INDEX IDX_72260B8A70A5F021 (an_order_id), INDEX IDX_72260B8A4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE licences (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prod_cat (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_D5E466B712469DE2 (category_id), INDEX IDX_D5E466B74584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prod_lic (id INT AUTO_INCREMENT NOT NULL, licence_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_95B22E4526EF07C9 (licence_id), INDEX IDX_95B22E454584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_addresses (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, address_id INT NOT NULL, INDEX IDX_6F2AF8F2A76ED395 (user_id), INDEX IDX_6F2AF8F2F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8A70A5F021 FOREIGN KEY (an_order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8A4584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE prod_cat ADD CONSTRAINT FK_D5E466B712469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE prod_cat ADD CONSTRAINT FK_D5E466B74584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE prod_lic ADD CONSTRAINT FK_95B22E4526EF07C9 FOREIGN KEY (licence_id) REFERENCES licences (id)');
        $this->addSql('ALTER TABLE prod_lic ADD CONSTRAINT FK_95B22E454584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE user_addresses ADD CONSTRAINT FK_6F2AF8F2A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_addresses ADD CONSTRAINT FK_6F2AF8F2F5B7AF75 FOREIGN KEY (address_id) REFERENCES addresses (id)');
        $this->addSql('DROP TABLE users_addresses');
        $this->addSql('ALTER TABLE addresses ADD country VARCHAR(50) NOT NULL, CHANGE city city VARCHAR(60) NOT NULL');
        $this->addSql('ALTER TABLE products DROP categories, DROP licences');
        $this->addSql('ALTER TABLE users ADD login VARCHAR(20) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prod_cat DROP FOREIGN KEY FK_D5E466B712469DE2');
        $this->addSql('ALTER TABLE prod_lic DROP FOREIGN KEY FK_95B22E4526EF07C9');
        $this->addSql('CREATE TABLE users_addresses (users_id INT NOT NULL, addresses_id INT NOT NULL, INDEX IDX_9B70FF767B3B43D (users_id), INDEX IDX_9B70FF75713BC80 (addresses_id), PRIMARY KEY(users_id, addresses_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE users_addresses ADD CONSTRAINT FK_9B70FF75713BC80 FOREIGN KEY (addresses_id) REFERENCES addresses (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_addresses ADD CONSTRAINT FK_9B70FF767B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE details');
        $this->addSql('DROP TABLE licences');
        $this->addSql('DROP TABLE prod_cat');
        $this->addSql('DROP TABLE prod_lic');
        $this->addSql('DROP TABLE user_addresses');
        $this->addSql('ALTER TABLE addresses DROP country, CHANGE city city VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE products ADD categories LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', ADD licences LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE users DROP login');
    }
}
