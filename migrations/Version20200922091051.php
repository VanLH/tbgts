<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200922091051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE prod_lic');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE prod_lic (id INT AUTO_INCREMENT NOT NULL, licence_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_95B22E4526EF07C9 (licence_id), INDEX IDX_95B22E454584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE prod_lic ADD CONSTRAINT FK_95B22E4526EF07C9 FOREIGN KEY (licence_id) REFERENCES licences (id)');
        $this->addSql('ALTER TABLE prod_lic ADD CONSTRAINT FK_95B22E454584665A FOREIGN KEY (product_id) REFERENCES products (id)');
    }
}
