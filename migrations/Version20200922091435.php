<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200922091435 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE products_licences (products_id INT NOT NULL, licences_id INT NOT NULL, INDEX IDX_7615BCFC6C8A81A9 (products_id), INDEX IDX_7615BCFC5EF2836 (licences_id), PRIMARY KEY(products_id, licences_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_addresses (users_id INT NOT NULL, addresses_id INT NOT NULL, INDEX IDX_9B70FF767B3B43D (users_id), INDEX IDX_9B70FF75713BC80 (addresses_id), PRIMARY KEY(users_id, addresses_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE products_licences ADD CONSTRAINT FK_7615BCFC6C8A81A9 FOREIGN KEY (products_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products_licences ADD CONSTRAINT FK_7615BCFC5EF2836 FOREIGN KEY (licences_id) REFERENCES licences (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_addresses ADD CONSTRAINT FK_9B70FF767B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_addresses ADD CONSTRAINT FK_9B70FF75713BC80 FOREIGN KEY (addresses_id) REFERENCES addresses (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE products_licences');
        $this->addSql('DROP TABLE users_addresses');
    }
}
