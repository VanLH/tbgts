<?php

namespace App\Controller;

use App\Entity\Addresses;
use App\Form\AddressesFormType;
use App\Repository\AddressesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddressController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function CreateAddress(Request $request): Response
    {

        $newAdress = new Addresses();
        $newAdress->addUser($this->getUser());
        $error = false;
        
        $formBuilder = $this->createForm(AddressesFormType::class,$newAdress);
        // dd($formBuilder);
        $formBuilder->handleRequest($request);


        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
 
            $foundAddress = $this->findAddress($newAdress);
            if($foundAddress){
                $error = true;
                return $this->render('addresses/create.html.twig', [
                    'addressForm' => $formBuilder->createView(),
                    'error' => $error
                ]);
            }

            // dd($error);
            $this->em->persist($newAdress);
            $this->em->flush();
            return $this->redirectToRoute('PageListAddresses');
        }

        return $this->render('addresses/create.html.twig', [
            'addressForm' => $formBuilder->createView(),
            'error' => $error
        ]);
    }

    public function ListAddresses(AddressesRepository $repo): Response
    {
        $userId = $this->getUser()->getId();
        $findAllAddresses = $repo->findByUser($userId);
        return $this->render('addresses/list.html.twig', [
            'listAddresses' => $findAllAddresses,
        ]);
    }

    public function findAddress(Addresses $address){
        $userId = $this->getUser()->getId();
        $repo= $this->em->getRepository(Addresses::class);

        $found = $repo->findBy(array(
            'street' => $address->getStreet(),
            'streetNumber' => $address->getStreetNumber(),
            'zipCode' => $address->getZipCode(),
            'country' => $address->getCountry(),
        ));

        return $repo->findByAddressIds($found,$userId);
    }

    public function UpdateAddress(Addresses $address, Request $request): Response
    {

        $formBuilder = $this->createFormBuilder($address)
            ->add('street', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('streetNumber', NumberType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('zipCode', TextType::class, [
                'required' => true,
                'attr' => [
                    'row' => 5,
                    'cols' => 10,
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'attr' => [
                    'row' => 5,
                    'cols' => 10,
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('additionalAddresse', TextType::class, [
                'required' => false,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('country', TextType::class, [
                'required' => true,
                'attr' => [
                    'row' => 5,
                    'cols' => 10,
                    'class' => 'form-control mb-4',
                ],
            ])
           
            ->getForm();

        $formBuilder->handleRequest($request);

        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
            // On récupère les images transmises
           
            $this->em->persist($address);
            $this->em->flush();
            return $this->redirectToRoute('PageListAddresses');
        }

        return $this->render('addresses/update.html.twig', [
            'addressForm' => $formBuilder->createView(),
        ]);
    }

    public function DeleteAddress(Addresses $address): Response
    {
        $this->em->remove($address);
        $this->em->flush();
        $repoAddresse = $this->em->getRepository(Addresses::class);

        $userId = $this->getUser()->getId();
        $findAllAddresses = $repoAddresse->findByUser($userId);

        return $this->render('addresses/list.html.twig', [
            'listAddresses' => $findAllAddresses,
            'alert' => true,
        ]);
    }


}
