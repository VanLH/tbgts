<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Repository\CategoriesRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends AbstractController
{
   
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function CreateCategory(Request $request): Response
    {
        $newCategorie = new Categories();

        $formBuilder = $this->createFormBuilder($newCategorie)
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])

            ->getForm();

        $formBuilder->handleRequest($request);

        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
           
            $this->em->persist($newCategorie);
            $this->em->flush();
            return $this->redirectToRoute('PageListCategories');
        }

        return $this->render('categories/create.html.twig', [
            'categoryForm' => $formBuilder->createView(),
        ]);
    }

    public function ListCategories(CategoriesRepository $repo): Response
    {
        $findAllCategory = $repo->findAll();

        return $this->render('categories/list.html.twig', [
            'listCategories' => $findAllCategory,
        ]);
    }

    public function UpdateCategory(Categories $category, Request $request): Response
    {
        $formBuilder = $this->createFormBuilder($category) // pour préremplir le form
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->getForm();

        $formBuilder->handleRequest($request);
        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {

            $this->em->persist($category);
            $this->em->flush();
            return $this->redirectToRoute('PageListCategories');
        }
        return $this->render('categories/update.html.twig', [
            'categoryForm' => $formBuilder->createView(),
        ]);
    }

    public function DeleteCategory(Categories $category): Response
    {
        $this->em->remove($category);
        $this->em->flush();
        $repoCategory = $this->em->getRepository(Categories::class);

        return $this->render('categories/list.html.twig', [
            'listCategories' => $repoCategory->findAll(),
            'alert' => true,
        ]);
    }
}
