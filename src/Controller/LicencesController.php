<?php

namespace App\Controller;


use App\Entity\Licences;
use App\Repository\LicencesRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LicencesController extends AbstractController
{
   
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function CreateLicence(Request $request): Response
    {
        $newLicence = new Licences();

        $formBuilder = $this->createFormBuilder($newLicence)
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])

            ->getForm();

        $formBuilder->handleRequest($request);

        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
           
            $this->em->persist($newLicence);
            $this->em->flush();
            return $this->redirectToRoute('PageListLicences');
        }

        return $this->render('licences/create.html.twig', [
            'licenceForm' => $formBuilder->createView(),
        ]);
    }

    public function ListLicences(LicencesRepository $repo): Response
    {
        $findAllLicence = $repo->findAll();

        return $this->render('licences/list.html.twig', [
            'listLicences' => $findAllLicence,
        ]);
    }

    public function UpdateLicence(Licences $licence, Request $request): Response
    {
        $formBuilder = $this->createFormBuilder($licence) // pour préremplir le form
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->getForm();

        $formBuilder->handleRequest($request);
        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {

            $this->em->persist($licence);
            $this->em->flush();
            return $this->redirectToRoute('PageListLicences');
        }
        return $this->render('licences/update.html.twig', [
            'licenceForm' => $formBuilder->createView(),
        ]);
    }

    public function DeleteLicence(Licences $licence): Response
    {
        $this->em->remove($licence);
        $this->em->flush();
        $repoLicence = $this->em->getRepository(Licences::class);

        return $this->render('licences/list.html.twig', [
            'listLicences' => $repoLicence->findAll(),
            'alert' => true,
        ]);
    }
}
