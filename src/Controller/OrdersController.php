<?php

namespace App\Controller;

use App\Entity\Products;
use App\Entity\Addresses;
use Doctrine\ORM\Query\Expr\Func;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


use App\Repository\AddressesRepository;


class OrdersController extends AbstractController
{
    /**
     * @Route("/orders", name="orders")
     */
    public function index()
    {
        return $this->render('orders/index.html.twig', [
            'controller_name' => 'OrdersController',
        ]);
    }

    public function ChooseDeliveryAddress(AddressesRepository $addressesRepo)
    {
        $userId = $this->getUser()->getId();
        $findAllAddresses = $addressesRepo->findByUser($userId);
        return $this->render('orders/listAddresses.html.twig', [
            'listAddresses' => $findAllAddresses,
        ]);
    }
    public function PayOrder(Request $request,AddressesRepository $addressesRepo )
    {

        $findAddress = $addressesRepo->findByAddressId($request->attributes->get('id'),$this->getUser()->getId());
        return $this->render('orders/finalDetail.html.twig', [
            'address' => $findAddress[0],
            'user' => $this->getUser(),
        ]);
    }

}
