<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Licences;
use App\Entity\Products;
use App\Repository\CategoriesRepository;
use App\Repository\LicencesRepository;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\Validator\Constraints\File;

class ProductsController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function CreateProduct(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $newProduct = new Products();

        $formBuilder = $this->createFormBuilder($newProduct)
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('price', TextType::class, [
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'row' => 5,
                    'cols' => 10,
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('reference', TextType::class, [
                'required' => true,
                'attr' => [
                    'row' => 5,
                    'cols' => 10,
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('quantity', IntegerType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('images', FileType::class, [
                'label' => 'Image',
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('categories', EntityType::class, [
                'class' => Categories::class,
                'query_builder' => function (CategoriesRepository $repo) {
                    return $repo
                        ->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                'multiple' => true,
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('licences', EntityType::class, [
                'class' => Licences::class,
                'query_builder' => function (LicencesRepository $repo) {
                    return $repo
                        ->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                'multiple' => true,
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->getForm();

        $formBuilder->handleRequest($request);

        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
            // On récupère les images transmises
            $images = $formBuilder->get('images')->getData();

            // On boucle sur les images
            $i=0;
            foreach ($images as $image) {
                // On génère un nouveau nom de fichier
                $fichier =
                    str_replace(' ', '_', $newProduct->getName()) .$i.
                    '.' .
                    $image->guessExtension();

                // On copie le fichier dans le dossier uploads
                $image->move($this->getParameter('images_directory'), $fichier);
                $i++;
            }

            // dd($newProduct);
            $this->em->persist($newProduct);
            $this->em->flush();
            return $this->redirectToRoute('PageListProducts');
        }

        return $this->render('products/create.html.twig', [
            'productForm' => $formBuilder->createView(),
        ]);
    }

    public function ListProducts(ProductsRepository $repo): Response
    {
        $extentionsFile = ['.png', '.jpeg', '.jpg'];
        $findAllProduct = $repo->findAll();

        foreach ($findAllProduct as $key => $value) {
            $nameFile = str_replace(' ', '_', $findAllProduct[$key]->getName());
            $nameFile= $nameFile.'0';

            for ($i = 0; $i < sizeof($extentionsFile); $i++) {
                if (
                    file_exists(
                        $this->getParameter('images_directory') .
                            '/' .
                            $nameFile .
                            $extentionsFile[$i]
                    )
                ) {
                    $findAllProduct[$key]->setImages([
                        $nameFile . $extentionsFile[$i],
                    ]);
                }
            }
        }
        // dd($findAllProduct);

        return $this->render('products/list.html.twig', [
            'listProducts' => $findAllProduct,
        ]);
    }

    public function ListProductsCat(ProductsRepository $repo,$cat): Response
    {
        
        $extentionsFile = ['.png', '.jpeg', '.jpg'];
        $findAllProductbyCat = $repo->findAllProductbyCat(str_replace('_',' ',$cat));

        foreach ($findAllProductbyCat as $key => $value) {
            $nameFile = str_replace(' ', '_', $findAllProductbyCat[$key]->getName());
            $nameFile= $nameFile.'0';

            for ($i = 0; $i < sizeof($extentionsFile); $i++) {
                if (
                    file_exists(
                        $this->getParameter('images_directory') .
                            '/' .
                            $nameFile .
                            $extentionsFile[$i]
                    )
                ) {
                    $findAllProductbyCat[$key]->setImages([
                        $nameFile . $extentionsFile[$i],
                    ]);
                }
            }
        }

        return $this->render('products/list.html.twig', [
            'listProducts' => $findAllProductbyCat,
        ]);
    }

    public function DetailsProduct(Products $product): Response
    {
        $tabImages = $this->findFiles($product);
        $tempTable = [];
        
        if($tabImages != []){
            
            for ($i = 0; $i < sizeof($tabImages); $i++) {
                array_push($tempTable,$tabImages[$i]);
            }
            $product->setImages($tempTable);
        }
        
        // dd($product);
        return $this->render('products/details.html.twig', [
            'productDetail' => $product,
        ]);
    }

    public function UpdateProduct(Products $product, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $oldProduct = $product;

        $formBuilder = $this->createFormBuilder($product) // pour préremplir le form
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control mb-4',
                ],
            ])
            ->add('price', TextType::class, [
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('reference', TextType::class, [
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('licences', EntityType::class, [
                'class' => Licences::class,
                'query_builder' => function (LicencesRepository $repo) {
                    return $repo
                        ->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                'multiple' => true,
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('images', FileType::class, [
                'label' => 'Image',
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->add('categories', EntityType::class, [
                'class' => Categories::class,
                'query_builder' => function (CategoriesRepository $repo) {
                    return $repo
                        ->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                'multiple' => true,
                'required' => true,
                'attr' => ['class' => 'form-control mb-4'],
            ])
            ->getForm();

        $formBuilder->handleRequest($request);
        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
            // On récupère les images transmises
            $images = $formBuilder->get('images')->getData();
            
            if( $images != [] ){
                // dd($images);

                $tabFiles = $this->findFiles($product);
                $this->deleteOldsImages($tabFiles);

                $i=0;
                foreach ($images as $image) {
                    $fichier =str_replace(' ', '_', $product->getName()) .$i.'.' .$image->guessExtension();
                    
                    $image->move($this->getParameter('images_directory'), $fichier);
                    $i++;
                }
            }
                
            $this->em->persist($product);
            $this->em->flush();
            return $this->redirectToRoute('PageListProducts');
        }
        // $this->removeCache();
        // dd($product->getName());
        return $this->render('products/update.html.twig', [
            'productForm' => $formBuilder->createView(),
            'productName' => $product->getName(),
        ]);
    }

    public function DeleteProduct(Products $product): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $tabFiles = $this->findFiles($product);

        $this->deleteOldsImages($tabFiles);

        $this->em->remove($product);
        $this->em->flush();
        $repoProduct = $this->em->getRepository(Products::class);

        return $this->render('products/list.html.twig', [
            'listProducts' => $repoProduct->findAll(),
            'alert' => true,
        ]);
    }

    public function deleteOldsImages($tabFiles)
    {
        $filesystem = new Filesystem();

        for ($i = 0; $i < sizeof($tabFiles); $i++) {

            $filesystem->remove(
                $this->getParameter('images_directory') .
                    '/' .$tabFiles[$i]
            );
        }
    }

    public function findFiles(Products $product){
        $finder = new Finder();
        $tabFiles = [];

        $nameFile = str_replace(' ', '_', $product->getName());
        $finder->files()->in($this->getParameter('images_directory'));
        $finder->files()->name($nameFile.'*');
        // dd($finder->hasResults());

        foreach ($finder as $key => $value) {
           array_push($tabFiles,$value->getFilename());
        }

        return $tabFiles;
    }

    public function findFilesArray($product){
        $finder = new Finder();
        $tabFiles = [];

        $nameFile = str_replace(' ', '_', $product[0]->getName());
        $finder->files()->in($this->getParameter('images_directory'));
        $finder->files()->name($nameFile.'*');
        // dd($finder->hasResults());

        foreach ($finder as $key => $value) {
           array_push($tabFiles,$value->getFilename());
        }

        return $tabFiles;
    }

    public function removeCache()
    {
        $filesystem   = $this->container->get('');
        $realCacheDir = $this->container->get('kernel.cache_dir')."/http_cache";
        $this->container->get('cache_clearer')->clear($realCacheDir);
        $filesystem->remove($realCacheDir);
    }
}
