<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\RegistrationFormType;
use App\models\RegisterFormModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class RegistrationController extends AbstractController
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function selectNumCient()
    {
        $repo = $this->em->getRepository(Users::class);

        if ($repo->findBy([], ['id' => 'DESC'], 1, 0) === []) {
            return 0;
        }
        $findLastUser = $repo->findBy([], ['id' => 'DESC'], 1, 0);
        return $findLastUser[0]->getNumClient();
    }

    
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $users = new Users();

        $lastNumClient = $this->selectNumCient();
        $numClient = intval(substr($lastNumClient, -3)) + 1;
        $numClientFinal = date("m").date("y") . '-' .sprintf("%03d",$numClient);
        $users->setNumClient($numClientFinal);

        $registerForm = new RegisterFormModel();
        // dd($use:rs);
        $form = $this->createForm(RegistrationFormType::class,$registerForm);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

      
            $users = $registerForm->toUser();
            $users->setNumClient($numClientFinal);
       
            $users->setPassword(
                $passwordEncoder->encodePassword(
                    $users,
                    $users->getPassword()
                )
                );  

            $this->em->persist($users);
            $this->em->flush();
            return $this->redirectToRoute('PageListProducts');

            // do anything else you need here, like send an email

        }

        return $this->render('register/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
