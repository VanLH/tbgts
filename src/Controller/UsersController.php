<?php

namespace App\Controller;

use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UsersController extends AbstractController{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function admin()
    {

        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();

        return $this->render('users/admin.html.twig', [
            'users' => $users
        ]);
    }

    public function DetailsUser(): Response
    {
        $user = $this->getUser();
        return $this->render('users/details.html.twig', [
            'userDetail' => $user,
        ]);
    }

    public function UpdateUser(Users $user, Request $request): Response
    {

        $formBuilder = $this->createFormBuilder($user)
        ->add('name', TextType::class, [
            'required' => true,
            'attr' => [
                'autofocus' => 'true',
                'class' => 'form-control',
            ],
        ])
        ->add('firstName', TextType::class, [
            'required' => true,
            'attr' => [
               
                'class' => 'form-control',
            ],
        ])
        ->add('phone', TextType::class, [
            'required' => false,
            'attr' => [
                'autofocus' => 'true',
                'class' => 'form-control',
            ],
        ])
        ->add('mobilePhone', TelType::class, [
            'required' => false,
            'attr' => [
                'autofocus' => 'true',
                'class' => 'form-control',
            ],
        ])
        ->add('birthDate', BirthdayType::class, [
            'widget'=> 'single_text',
            'html5'=> false,
            'required' => true,
            'attr' => [
                'class' => 'form-control js-datepicker',
            ],
        ])
        ->add('email',EmailType::class,[
            'attr' => [
                'class' => 'form-control',
            ],
        ])
        ->getForm();

        $formBuilder->handleRequest($request);

        // dd($request);
        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
        // dd($user);
            
            $this->em->persist($user);
            $this->em->flush();
            return $this->redirectToRoute('PageDetailsUser');
        }
        // else{
        //     dd($user);
        // }

        return $this->render('users/update.html.twig', [
            'userForm' => $formBuilder->createView(),
        ]);
    }
}
