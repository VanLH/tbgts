<?php

namespace App\Entity;

use App\Repository\DetailsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DetailsRepository::class)
 */
class Details
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Orders::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $anOrder;

    /**
     * @ORM\ManyToOne(targetEntity=Products::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="float")
     */
    private $totalInvoiced;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnOrder(): ?Orders
    {
        return $this->anOrder;
    }

    public function setAnOrder(?Orders $anOrder): self
    {
        $this->anOrder = $anOrder;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getTotalInvoiced(): ?float
    {
        return $this->totalInvoiced;
    }

    public function setTotalInvoiced(float $totalInvoiced): self
    {
        $this->totalInvoiced = $totalInvoiced;

        return $this;
    }
}
