<?php

namespace App\Entity;

use App\Repository\OrdersRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrdersRepository::class)
 */
class Orders
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $orderDate;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="float")
     */
    private $totalVAT;

    /**
     * @ORM\Column(type="float")
     */
    private $totalWithVAT;

    /**
     * @ORM\Column(type="float")
     */
    private $delivery;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderDate(): ?\DateTimeInterface
    {
        return $this->orderDate;
    }

    public function setOrderDate(\DateTimeInterface $orderDate): self
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getTotalVAT(): ?float
    {
        return $this->totalVAT;
    }

    public function setTotalVAT(float $totalVAT): self
    {
        $this->totalVAT = $totalVAT;

        return $this;
    }

    public function getTotalWithVAT(): ?float
    {
        return $this->totalWithVAT;
    }

    public function setTotalWithVAT(float $totalWithVAT): self
    {
        $this->totalWithVAT = $totalWithVAT;

        return $this;
    }

    public function getDelivery(): ?float
    {
        return $this->delivery;
    }

    public function setDelivery(float $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }
}
