<?php

namespace App\Entity;

use App\Repository\ProductsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductsRepository::class)
 */
class Products
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

   
    /**
     * @ORM\Column(type="string", length=120)
     */
    private $reference;


    
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity=Categories::class, inversedBy="products", cascade={"persist"})
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity=Licences::class, inversedBy="products", cascade={"persist"})
     */
    private $licences;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->licences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setImages(array $images): self
    {
        $this->images = $images;

        return $this;
    }  

    /**
     * @return Collection|Categories[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategories(Categories $categories): self
    {
        if (!$this->categories->contains($categories)) {
            $this->categories[] = $categories;
        }

        return $this;
    }

    public function removeCategories(Categories $categories): self
    {
        if ($this->categories->contains($categories)) {
            $this->categories->removeElement($categories);
        }

        return $this;
    }

    /**
     * @return Collection|Licences[]
     */
    public function getLicences(): Collection
    {
        return $this->licences;
    }

    public function addLicence(Licences $licence): self
    {
        if (!$this->licences->contains($licence)) {
            $this->licences[] = $licence;
        }

        return $this;
    }

    public function removeLicence(Licences $licence): self
    {
        if ($this->licences->contains($licence)) {
            $this->licences->removeElement($licence);
        }

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
