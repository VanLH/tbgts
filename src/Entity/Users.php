<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Users implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $numClient;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $password;

    /**
     * @ORM\Column(type="date")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $mobilePhone;

    /**
     * @ORM\Column(type="date")
     */
    private $firstLogon;

    /**
     * @ORM\Column(type="date")
     */
    private $lastLogon;


    //creer adresse

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $login;


    // private $roles = [];

    /**
     * @ORM\ManyToMany(targetEntity=Addresses::class, inversedBy="users", cascade={"persist"})
     */
    private $addresses;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    // /**
    //  * @ORM\PrePersist()
    //  */
    // public function initNumClient() {

    // }

    /**
     * @ORM\PrePersist()
     */
    public function initDate() {
        $this->lastLogon = new DateTime();
        $this->firstLogon = new DateTime();
    }

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getNumClient(): ?string
    {
        return $this->numClient;
    }

    public function setNumClient(string $numClient): self
    {
        $this->numClient = $numClient;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    
    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }
    
    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;
        
        return $this;
    }
    
    public function getPhone(): ?string
    {
        return $this->phone;
    }
    
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        
        return $this;
    }
    
    public function getMobilePhone(): ?string
    {
        return $this->mobilePhone;
    }
    
    public function setMobilePhone(?string $mobilePhone): self
    {
        $this->mobilePhone = $mobilePhone;
        
        return $this;
    }
    
    public function getFirstLogon(): ?\DateTimeInterface
    {
        return $this->firstLogon;
    }
    
    public function setFirstLogon(\DateTimeInterface $firstLogon): self
    {
        $this->firstLogon = $firstLogon;
        
        return $this;
    }
    
    public function getLastLogon(): ?\DateTimeInterface
    {
        return $this->lastLogon;
    }
    
    public function setLastLogon(\DateTimeInterface $lastLogon): self
    {
        $this->lastLogon = $lastLogon;
        
        return $this;
    }
    
    public function getLogin(): ?string
    {
        return $this->login;
    }
    
    public function setLogin(string $login): self
    {
        $this->login = $login;
        
        return $this;
    }
    
    /**
     * @return Collection|Addresses[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }
    
    public function addAddress(Addresses $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
        }
        
        return $this;
    }
    
    public function removeAddress(Addresses $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
        }
        
        return $this;
    }
    
    public function setRoles(string $roles): self
    {
        if (!in_array($roles, $this->roles)) {
            $this->roles[] = $roles;
        }

        return $this;
    }
    
    public function getRoles(){
        
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    // public function setRoles(array $roles): self
    // {
    //     dd('taga');

    //     $this->roles = $roles;

    //     return $this;
    // }
    
    
    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string|null The encoded password if any
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    
    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(){}

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername(){
        $this->getLogin();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(){}
}
