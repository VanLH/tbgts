<?php

namespace App\Form;

use App\Entity\Addresses;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressesFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // dd('tada');
        $builder
            ->add('street', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control',
                ],
            ])
            ->add('streetNumber', NumberType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control',
                ],
            ])
            ->add('zipCode', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('additionalAddresse', TextType::class, [
                'required' => false,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control',
                ],
            ])
            ->add('country', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ],
            ]);
            // dd('tada');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Addresses::class,
        ]);
    }
}
