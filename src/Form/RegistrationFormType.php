<?php

namespace App\Form;

use App\Entity\Addresses;
use App\Entity\Users;
use App\models\RegisterFormModel;
use DateTime;
use Doctrine\DBAL\Types\ArrayType;
use Doctrine\DBAL\Types\SimpleArrayType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $todayDate = DateTime::createFromFormat('j-m-Y', strval(date('d-m-Y')));

        // dd($options['data']->getNumClient());

        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control',
                ],
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'attr' => [
                   
                    'class' => 'form-control',
                ],
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control',
                ],
            ])
            ->add('mobilePhone', TelType::class, [
                'required' => false,
                'attr' => [
                    'autofocus' => 'true',
                    'class' => 'form-control',
                ],
            ])
            ->add('birthDate', DateType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            // ->add('firstLogon', HiddenType::class, [
            //     'empty_data' => $todayDate,
            // ])
            // ->add('lastLogon', HiddenType::class, [
            //     'empty_data' => $todayDate,
            // ])
            // ->add('numClient', HiddenType::class, [
            //     'empty_data' => $options['data']->getNumClient(),
            // ])
            ->add('email',EmailType::class,[
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('login', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 2,
                        'minMessage' =>
                            'Votre nom d\'utilisateur doit contenir au moins {{ limit }} caractères.',
                    ]),
                ],
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('password', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                // 'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' =>
                            'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('address', AddressesFormType::class)
            // 
            ;
            // dd(gettype($builder));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RegisterFormModel::class,
        ]);
    }
}
