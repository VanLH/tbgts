<?php

namespace App\Repository;

use App\Entity\Addresses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Addresses|null find($id, $lockMode = null, $lockVersion = null)
 * @method Addresses|null findOneBy(array $criteria, array $orderBy = null)
 * @method Addresses[]    findAll()
 * @method Addresses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Addresses::class);
    }

    /**
     * @return Addresses[] Returns an array of Addresses objects
     */
    
    public function findByUser($idUser) {
        $query = $this->getEntityManager()->createQuery('SELECT a FROM  App\Entity\Addresses a inner join a.users ua where ua.id = :idUsers');
        $query->setParameter('idUsers', $idUser);
        return $query->getResult();
    }

    public function findByAddressIds($address,$userId) {

        $tab=[];
        // dd($address);
        for($i=0; $i<sizeof($address);$i++){
            array_push($tab,$address[$i]->getId());
        }
        
        $idsAddress = implode(",", $tab);
        // dd($idsAddress);

        $query = $this->getEntityManager()->createQuery('SELECT a FROM  App\Entity\Addresses a inner join a.users ua where a.id IN (:idsAddress) and ua.id=:userId');
        $query->setParameter('idsAddress', $idsAddress);
        $query->setParameter('userId', $userId);
        // dd($query->getResult());
        return $query->getResult();
    }
    
    public function findByAddressId($address,$userId) {

        $query = $this->getEntityManager()->createQuery('SELECT a FROM  App\Entity\Addresses a inner join a.users ua where a.id=:idAddress and ua.id=:userId');
        $query->setParameter('idAddress', $address);
        $query->setParameter('userId', $userId);
        // dd($query->getResult());
        return $query->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Addresses
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
