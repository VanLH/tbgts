<?php

namespace App\Repository;

use App\Entity\ProdLic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProdLic|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProdLic|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProdLic[]    findAll()
 * @method ProdLic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProdLicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProdLic::class);
    }

    // /**
    //  * @return ProdLic[] Returns an array of ProdLic objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProdLic
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
