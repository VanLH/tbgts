<?php

namespace App\models;

use App\Entity\Users;

class RegisterFormModel {
    public $name;
    public $firstName;
    public $phone;
    public $mobilePhone;
    public $birthDate;
    public $email;
    public $login;
    public $password;
    public $address;

    public function toUser() {
        $user = new Users();

        $user->setName($this->name);
        $user->setFirstName($this->firstName);
        $user->setPhone($this->phone);
        $user->setMobilePhone($this->mobilePhone);
        $user->setBirthDate($this->birthDate);
        $user->setEmail($this->email);
        $user->setLogin($this->login);
        $user->setPassword($this->password);

        $user->addAddress($this->address);

        return $user;
    }
}